﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace IdentityServer
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            #region IdentityServerSetup
            //Инциализация IS
            services.AddIdentityServer()
                //Сертификат разработчика
                .AddDeveloperSigningCredential()
                // Клиенты котором мы можем выдовать доступ
                .AddInMemoryClients(Config.GetClients())
                //Ресурс который описывает пользователя
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                //Ресурс который защишаем
                .AddInMemoryApiResources(Config.GetApiResources())
                //Тестовые пользователи которым даем доступ (необъязательно)
                .AddTestUsers(Config.GetTestUsers());

            // http://localhost:5000/.well-known/openid-configuration   тест IS
            #endregion

            #region Token Settings

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(opt =>
                {
                    opt.Authority = "http://localhost:5000";
                    opt.RequireHttpsMetadata = false;
                    opt.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = "http://localhost:5000",
                        ValidateAudience = false,
                        ValidAudience = "webApiClient",//test purposes
                        ValidateLifetime = true,
                    };
                });
            #endregion
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //Добавляем в конвейр
            app.UseIdentityServer();
        }
    }
}
