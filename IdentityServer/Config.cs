﻿using System.Collections.Generic;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace IdentityServer
{
    public class Config
    {
        //Client
        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                #region Hybrid GrantTypes

                new Client
                {
                    ClientId = "webApiClient",
                    ClientName = "Test Hybrid Grant Type",
                    AllowedGrantTypes = GrantTypes.Hybrid,

                    ClientSecrets =
                    {
                        new Secret("superKey".Sha256())
                    },
                    
                    //область где клиенту разрешено запрашивать 
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "webApiClient"
                    },
                    //refresh token
                    AllowOfflineAccess = true,
                }
                #endregion
               
            };
        }

        //Resource
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("webApiClient", "Test Client")
            };
        }

        public static List<TestUser> GetTestUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "",
                    Username = "noyan",
                    Password = "123",
                    Claims = new List<Claim>
                    {
                        new Claim(JwtClaimTypes.Email,"asd@gmail.com"),
                        new Claim(JwtClaimTypes.Role, "admin")
                    }
                }
            };
        }
    }
}