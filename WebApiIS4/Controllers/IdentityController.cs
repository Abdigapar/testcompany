﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Microsoft.AspNetCore.Mvc;

namespace WebApiIS4.Controllers
{
    [Route("api/identity")]
    public class IdentityController : ControllerBase
    {
        [HttpGet]
        public async Task<IActionResult> RequestTokenAsync()
        {
            var disco = await DiscoveryClient.GetAsync("http://localhost:5000");
          
            var tokenClient = new TokenClient(disco.TokenEndpoint, "webApiClient", "superKey");
           // var tokenResponse = await tokenClient.RequestClientCredentialsAsync("webApiClient");
           var tokenResponse = await tokenClient.RequestClientCredentialsAsync("webApiClient");

            if (tokenResponse.IsError)
            {
                return new JsonResult(tokenResponse.Error);
            }

            return new JsonResult(tokenResponse.Json);
        }
    }
}