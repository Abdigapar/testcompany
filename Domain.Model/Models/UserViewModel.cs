﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Model.Models
{
    public class UserViewModel
    {
        // Создаем Вью модель нашего Юзер моделя
        public int Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
    }
}
