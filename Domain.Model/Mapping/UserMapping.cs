﻿using AutoMapper;
using Domain.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Model.Mapping
{
    public class UserMapping : Profile
    {
        public UserMapping()
        {
            // Маппим два объекта .
            CreateMap<User, UserViewModel>()
                .ForMember(q => q.Id, q => q.MapFrom(c => c.Id))
                .ForMember(q => q.Name, q => q.MapFrom(c => c.FirstName))
                .ForMember(q => q.Login, q => q.MapFrom(c => c.Email));
        }
    }
}
