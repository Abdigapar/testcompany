﻿using Domain.Model.Models;
using FluentAssertions;
using FluentValidation;

namespace Domain.Model.Validator
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(c => c.FirstName).NotEmpty().MaximumLength(100).MinimumLength(2);
            RuleFor(c => c.Email).NotEmpty().EmailAddress();
            RuleFor(c => c.LastName).NotEmpty().MaximumLength(100).MinimumLength(2);
            RuleFor(c => c.Password).NotEmpty();
            RuleFor(c => c.ConfirmPassword).Equal(c => c.Password).NotEmpty();
            RuleFor(c => c.Email).Should().NotBe(CascadeMode);
        }
    }
}