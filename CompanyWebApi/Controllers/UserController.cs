﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Domain.Model.Mapping;
using Domain.Model.Models;
using Infrastructure.EntityFramework.Contexts;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;

namespace CompanyWebApi.Controllers
{
    // Путь к контроллеру http://localhost:****/api/user  
    //v1
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly CompanyContext _context;
        private readonly IMapper _mapping;

        public UserController(CompanyContext context , IMapper mapping)
        {
            _context = context;
            _mapping = mapping;
        }

        // Вытаскиваем всех юзеров в экран
        [HttpGet]
        public ActionResult<IEnumerable<User>> GetMapping()
        {
            var userToList = _context.Users.ToList();
            var userMapping = _mapping.Map<IEnumerable<UserViewModel>>(userToList);

            return Ok(userMapping);
        }

        // Вытасиваем по Id определенного юзера
        [HttpGet("{id}")]
        public ActionResult<IEnumerable<User>> GetId(int id)
        {
            User user = _context.Users.FirstOrDefault(c => c.Id == id);
            if (user == null)
            {
                return BadRequest();
            }

            if (ModelState.IsValid)
            {
                
            }

            return Ok(user);
        }

        // Добавляем нового пользователя
        [HttpPost]
        public IActionResult Post([FromBody] User user)
        {
            if (user == null)
            {
                return BadRequest();
            }

            _context.Users.Add(user);
            _context.SaveChanges();
            return Ok(user);
        }

        // Находим по Id юзера и меняем ему свойства 
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] User user)
        {
            // Находим по Id в базе пользователя
            var findUser = _context.Users.FirstOrDefault(c => c.Id == id);

            // Если результат нашего id равен Null выдаем ошибку 
            if (findUser == null)
            {
                return BadRequest("Пользователь не найден");
            }

            // изменяем знаяения Юзера
            findUser.Email = user.Email;
            findUser.FirstName = user.FirstName;
            findUser.LastName = user.LastName;
            findUser.Password = user.Password;

            _context.Users.Update(findUser);
            _context.SaveChanges();
            return Ok(findUser);
        }

        // Удаляем по Id юзера
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            User user = _context.Users.FirstOrDefault(c => c.Id == id);

            if (user == null)
            {
                BadRequest("Пользователь не найден");
            }

            _context.Users.Remove(user);
            _context.SaveChanges();

            StatusCode(200);
        }
    }
}