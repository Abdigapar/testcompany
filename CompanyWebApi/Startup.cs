﻿using System.Reflection;
using AutoMapper;
using Domain.Model.Mapping;
using Domain.Model.Validator;
using FluentValidation.AspNetCore;
using Infrastructure.EntityFramework.Contexts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace CompanyWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CompanyContext>(opt => opt.UseSqlServer(Configuration.GetConnectionString("CompanyTest")));

            // добавили в DependencyInjection сваггер и поменяли в LaunchSettings.Json => launchUrl / Url на swagger
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "TestCompany service",
                    Description = "TestCompany service API Swagger",
                });
            });
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddFluentValidation(fvc => fvc.RegisterValidatorsFromAssemblyContaining<UserValidator>());


            // Добавили Маппер и также скачали библиотеку AutoMapper / AutoMapper.Extensions.Microsoft.DependencyInjection
            services.AddAutoMapper(typeof(UserMapping).GetTypeInfo().Assembly);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            // Добавили в конвейер сваггер 
            app.UseSwagger();
            app.UseSwaggerUI(s =>
            {
                s.SwaggerEndpoint("/swagger/v1/swagger.json", "User service project API v1.0");
                s.DocumentTitle = "User service project API v1.0";
                s.DocExpansion(DocExpansion.None);
            });

            app.UseMvc();
        }
    }
}
