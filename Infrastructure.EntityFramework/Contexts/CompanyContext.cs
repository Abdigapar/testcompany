﻿using Domain.Model.Models;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.EntityFramework.Contexts
{
    public sealed class CompanyContext : DbContext
    {
        // Добавляем к базе данных Таблицу Users
        // FliliyaTest => Таблица => Users
        public DbSet<User> Users { get; set; }

        public CompanyContext(DbContextOptions<CompanyContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}